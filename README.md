# pkg-rocm-tools

This builds a number of packages with utilities that facilitate working with
GPUs in environments isolated from the host system.

This currently builds three packages:

- **pkg-rocm-tools**
- **rocm-qemu-support**
- **rocm-podman-support**

## pkg-rocm-tools

These are utilities and files meant to assist Debian contributors in the
process of adding support for ROCm in their packages:

- **rocm-test-launcher**
  Launches tests making use of AMD GPUs, and optionally exports system
  information useful for debugging GPU issues. Use this to skip tests on
  [ci.debian.net](https://ci.debian.net) (no GPUs), rather than fail them.

## rocm-qemu-support

These are utilities for working with ROCm inside QEMU VMs:

- **rocm-qemu-setup**
  Prepare the system for GPU pass-through
- **rocm-qemu-create**
  Build QEMU images with GPU pass-through support
- **rocm-qemu-run**
  Boot a VM with GPU pass-through
- **autopkgtest-virt-qemu+rocm**
  autopkgtest backend that extends the regular backend with automatic setup of
  GPU pass-through

## rocm-podman-support

These are utilities for working with ROCm inside Podman containers:

- **rocm-podman-setup**
  Prepare the system for GPU-in-container use
- **rocm-podman-create**
  Build podman images with GPU support
- **rocm-podman-run**
  Run a command in a container with GPU support
- **autopkgtest-virt-podman+rocm**
  autopkgtest backend that extends the regular backend with automatic setup
  of devices and subuid mappings.

## Examples

```shell
# Run setup to determine what changes your system needs
$ rocm-qemu-setup

# Alternatively, if you want to run as a user
$ rocm-qemu-setup -u somename


# Create default image for unstable
$ sudo rocm-qemu-create /var/tmp/unstable.img

# Live above, but include an SSH key
$ sudo rocm-qemu-create -a /path/to/authorized_keys

# Create image for bookworm, using a fast local mirror (eg APT cache)
$ sudo rocm-qemu-create -r bookworm -m http://10.1.2.3:9999/debian

# If you want a regular user to run the image
$ sudo chown <user>: /var/tmp/unstable.img


# Run rocrand's autopkgtests (from the official archive) in that image
$ autopkgtest -B rocrand -- qemu+rocm /var/tmp/unstable.img

# By default, the qemu+rocm backend will claim 75% of host cores and RAM. Use
# --cpus and --ram-size to adjust:
$ autopkgtest -B rocrand -- qemu+rocm --cpus 4 --ram-size 16384 /var/tmp/unstable.img

# Run rocrand's autopkgtest, passing in only the GPU at 2b:00.0
$ autopkgtest -B rocrand -- qemu+rocm --gpu 2b:00.0 /var/tmp/unstable.img


# Boot into the image if you need to, here with a shared directory
$ rocm-qemu-run -d /dir/on/host /var/tmp/unstable.img
```
